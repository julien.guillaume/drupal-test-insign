<?php

namespace Drupal\restriction_login_insign\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Codes de validation entity.
 *
 * @ingroup restriction_login_insign
 *
 * @ContentEntityType(
 *   id = "validation_code_entity",
 *   label = @Translation("Codes de validation"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\restriction_login_insign\CodeEntityListBuilder",
 *     "views_data" = "Drupal\restriction_login_insign\Entity\CodeEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\restriction_login_insign\Form\CodeEntityForm",
 *       "add" = "Drupal\restriction_login_insign\Form\CodeEntityForm",
 *       "edit" = "Drupal\restriction_login_insign\Form\CodeEntityForm",
 *       "delete" = "Drupal\restriction_login_insign\Form\CodeEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\restriction_login_insign\CodeEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\restriction_login_insign\CodeEntityAccessControlHandler",
 *   },
 *   base_table = "validation_code_entity",
 *   translatable = FALSE,
 *   admin_permission = "administer codes de validation entities",
 *   entity_keys = {
 *     "id" = "code",
 *     "label" = "code",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/validation_code_entity/{validation_code_entity}",
 *     "add-form" = "/admin/structure/validation_code_entity/add",
 *     "edit-form" = "/admin/structure/validation_code_entity/{validation_code_entity}/edit",
 *     "delete-form" = "/admin/structure/validation_code_entity/{validation_code_entity}/delete",
 *     "collection" = "/admin/structure/validation_code_entity",
 *   },
 *   field_ui_base_route = "validation_code_entity.settings"
 * )
 */
class CodeEntity extends ContentEntityBase implements CodeEntityInterface {

  public function getCode() {
    return $this->get('code')->value;
  }

  public function setCode($name) {
    $this->set('code', $name);
    return $this;
  }

  public function getUsed() {
    return $this->get('used')->value;
  }

  public function setUsed($used) {
    $this->set('used', $used);
    return $this;
  }

    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Code'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['used'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t("Code utilisé"))
        ->setDisplayConfigurable('form', FALSE);

    return $fields;
  }

}
