<?php

namespace Drupal\restriction_login_insign\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Codes de validation entities.
 *
 * @ingroup restriction_login_insign
 */
interface CodeEntityInterface extends ContentEntityInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Codes 
   *
   * @return string
   *   code
   */
  public function getCode();

  /**
   * Sets the Codes .
   *
   * @param string $code
   *   The Codes .
   *
   * @return \Drupal\restriction_login_insign\Entity\CodeEntityInterface
   *   The called Codes de validation entity.
   */
  public function setCode($name);

  public function setUsed($used);
  public function getUsed();

    
}
