<?php

namespace Drupal\restriction_login_insign\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Codes de validation entities.
 */
class CodeEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
