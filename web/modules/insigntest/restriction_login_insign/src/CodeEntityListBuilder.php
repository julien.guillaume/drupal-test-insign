<?php

namespace Drupal\restriction_login_insign;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Codes de validation entities.
 *
 * @ingroup restriction_login_insign
 */
class CodeEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {

    $header['code'] = $this->t('Code');
    $header['used'] = $this->t('Code utilisé');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\restriction_login_insign\Entity\CodeEntity $entity */
    $row['code'] = $entity->id();
    $row['used'] = $entity->getUsed();
    return $row + parent::buildRow($entity);
  }
}
