<?php

namespace Drupal\restriction_login_insign;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Codes de validation entity.
 *
 * @see \Drupal\restriction_login_insign\Entity\CodeEntity.
 */
class CodeEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\restriction_login_insign\Entity\CodeEntityInterface $entity */

    switch ($operation) {

      case 'view':

        return AccessResult::allowedIfHasPermission($account, 'view published codes de validation entities');

      case 'update':

          return AccessResult::forbidden();//AccessResult::allowedIfHasPermission($account, 'edit codes de validation entities');

      case 'delete':
          if($entity->getUsed()!=1)
              return  AccessResult::allowedIfHasPermission($account, 'delete codes de validation entities');
          else
              return  AccessResult::forbidden();
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add codes de validation entities');
  }


}
