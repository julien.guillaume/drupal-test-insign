<?php
namespace Drupal\restriction_login_insign\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

class CodeValidationController extends ControllerBase {

    
  public function export() {
  
      $handle = fopen('php://temp', 'w+');
      $header = [
          'Code',
          'Utilisateur email',
      ];
      fputcsv($handle, $header);
      $entityStorage = \Drupal::entityTypeManager()->getStorage('validation_code_entity');
      $query = $entityStorage->getQuery();
      $ids = $query->condition('used', true)
              ->execute();

      $codes = $entityStorage->loadMultiple($ids);
      $userStorage = \Drupal::entityTypeManager()->getStorage('user');      

      foreach ($codes as $code) {
          $query = $userStorage->getQuery();
          $userid = $query->condition('field_code_de_validation', $code->id())
                          ->execute();

          if(count($userid)>0){
              $user = $userStorage->load(array_keys($userid)[0]);
              $data = [$code->getCode(),$user->getEmail()];
              fputcsv($handle, array_values($data));              
          }          
      }
      rewind($handle);
      
      $csv_data = stream_get_contents($handle);
      fclose($handle);

      $response = new Response();
      $response->headers->set('Content-Type', 'text/csv');
      $response->headers->set('Content-Disposition', 'attachment; filename="code-report.csv"');
      
      $response->setContent($csv_data);
      
      return $response;
  }

}
