<?php

namespace Drupal\restriction_login_insign\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Codes de validation entities.
 *
 * @ingroup restriction_login_insign
 */
class CodeEntityDeleteForm extends ContentEntityDeleteForm {


}
