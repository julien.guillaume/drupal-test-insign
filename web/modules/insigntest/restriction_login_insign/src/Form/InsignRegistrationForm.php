<?php

namespace Drupal\restriction_login_insign\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\RegisterForm;
use Drupal\restriction_login_insign\Entity\CodeEntity;
/**
 * Class InsignRegistrationForm.
 */
class InsignRegistrationForm extends RegisterForm {

    
    public function buildForm(array $form, FormStateInterface $form_state) {
        $form = parent::buildForm($form,$form_state);
        $form['validationCode'] = array(
            '#type' => 'textfield',
            '#title' => $this->t("Code de validation"),
            '#default_value' => "",
            '#size' => 60,
            '#maxlength' => 128,
            '#required' => TRUE,
        );
        return $form;
    }

    private function findAvailableCodeId($code){
        $entity = \Drupal::entityTypeManager()->getStorage('validation_code_entity');
        $query = $entity->getQuery();
        $ids = $query->condition('used', false)
                     ->condition('code', $code)
              ->execute();

        if(count($ids) > 0)
            return array_keys($ids)[0];

        return null;
    }
    public function validateForm(array &$form, FormStateInterface $form_state) {
      $config = $this->config('restriction_login_insign.settings');

        parent::validateForm($form, $form_state);

        $codeToCheck = $form_state->getValue("validationCode") ;

        if( ( $codeToCheck != $config->get('default_validation_code')) && empty($this->findAvailableCodeId($codeToCheck)) ){
            $form_state->setErrorByName('validationCode', $this->t('Code invalide'));
        }        

    }
    
    public function save(array $form, FormStateInterface $form_state) {

        $codeToCheck = $form_state->getValue("validationCode") ;
        $id = $this->findAvailableCodeId($codeToCheck);
        if($id){
            
            $codeEntity = CodeEntity::load($id);
            $codeEntity->setUsed(true);
            $codeEntity->save();
            
        }
        
        $account = $this->entity;
        $account->set('field_code_de_validation',$id);
        $account->activate();

        

        $this
            ->messenger()
            ->addStatus($this
                        ->t('Merci pour votre demande de compte. Votre compte est a été valider automatiquement en phase de test. '));
              $form_state
                  ->setRedirect('<front>');
        parent::save($form,$form_state);
    }
}
