<?php

namespace Drupal\restriction_login_insign\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  const SETTINGS = 'restriction_login_insign.settings';
  protected function getEditableConfigNames() {
      return [
          static::SETTINGS,
      ];
  }

  public function getFormId() {
    return 'restriction_login_insign_settings_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
      $config = $this->config('restriction_login_insign.settings');
      
      $form['default_validation_code'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Default validation code'),
          '#default_value' => $config->get('default_validation_code')
      ];
      
      return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
      $this->config('restriction_login_insign.settings')
           ->set('default_validation_code', $form_state->getValue('default_validation_code'))
           ->save();
      parent::submitForm($form, $form_state);
  }

}
