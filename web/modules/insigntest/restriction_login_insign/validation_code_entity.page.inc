<?php

/**
 * @file
 * Contains validation_code_entity.page.inc.
 *
 * Page callback for Codes de validation entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Codes de validation templates.
 *
 * Default template: validation_code_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_validation_code_entity(array &$variables) {
  // Fetch CodeEntity Entity Object.
  $validation_code_entity = $variables['elements']['#validation_code_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
